<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー編集画面</title>
        <link href="./CSS/usereditstyle.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <h1>ユーザー編集</h1>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                            </li>
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>
            <div class="useredit">
            <form action="useredit" method="post"><br />
                <input name="id" value="${editUser.id}" id="id" type="hidden" >
                               
                <label for="login_id">ログインＩＤ(半角英数字６～２０文字)</label> <br />
                <input name="login_id" value="${editUser.login_id}" id="login_id" class="login_id"/> <br/>
                
                <label for="password">パスワード(半角英数字記号６～２０文字)</label> <br />
                <input name="password" type="password" id="password" class="password" /> <br />
                
                <label for="passwordConfirm">パスワード(確認用)</label> <br />
                <input name="passwordConfirm" type="password" id="passwordConfirm" class="passwordConfirm"/> <br/>
                
                <label for="name">名前(１０文字以内)</label> <br />
                <input name="name" value="${editUser.name}" id="name" class="name"/> <br />
                
                <c:if test="${loginUser.id != editUser.id}">
                <label for="store_id">支店</label> <br />
                    <select name="store_id">
                        <c:forEach items="${stores}" var="store" >
                            <c:if test="${ store.id == editUser.store_id }">
                                <option value="${store.id}" selected>${store.name}</option>
                            </c:if>
                            <c:if test="${ store.id != editUser.store_id }">
                                <option value="${store.id}" >${store.name}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                    <br />
                <label for="position_id">部署・役職</label> <br />
                    <select name="position_id">
                        <c:forEach items="${positions}" var="position" >
                            <c:if test="${ position.id == editUser.position_id }">
                                <option value="${position.id}" selected>${position.name}</option>
                            </c:if>
                            <c:if test="${ position.id != editUser.position_id }">
                                <option value="${position.id}" >${position.name}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                    <br />
                </c:if>
                <c:if test="${loginUser.id == editUser.id}">
                    <input name="store_id" value="${loginUser.store_id}" type="hidden" />
                    <input name="position_id" value="${loginUser.position_id}" type="hidden" />
                </c:if> <br/>             
                <input type="submit" value="登録" class="submit" /> <br />
                <a href="usersetting" class="return">戻る</a>
            </form>
            </div>
            <c:remove var="editUser" scope="session" />
        </div>
        
    </body>
</html>