<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>新規投稿画面</title>
        <link href="./CSS/messagestyle.css" rel="stylesheet" type="text/css">
    </head>
    <body>
       <h1>新規投稿</h1>
       <div class="main-contents">
          <c:if test="${ not empty errorMessages }">
              <div class="errorMessages">
                  <ul>
                      <c:forEach items="${errorMessages}" var="message">
                          <li><c:out value="${message}" />
                      </c:forEach>
                  </ul>
              </div>
              <c:remove var="errorMessages" scope="session"/>
          </c:if>
          <div class="message" >
              <form action="message" method="post">

                 <div class="title" >
                 <label for="title">タイトル（３０文字以内）</label> <br />
                 <input type="text" name="title" value="${title}" class="title" /> <br />
                 </div>

                 <div class="category" >
                 <label for="category">カテゴリー（１０文字以内）</label> <br />
                 <input type="text" name="category" value="${category}" class="category" /> <br />
                 </div>

                 <div class="text" >
                 <label for="text">テキスト（１０００文字以内）</label> <br />
                 <textarea name="text" cols="100" rows="10" id="text" class="text" >${text}</textarea> <br />
                 </div>

                 <input type="submit" value="投稿" class="submit" /> <br />
                 <a href="./" class="return">戻る</a>
              </form>
          </div>
          <c:remove var="title" scope="session"/>
          <c:remove var="category" scope="session"/>
          <c:remove var="text" scope="session"/>
       </div>

</body>
</html>