<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ログイン画面</title>
		<link href="./CSS/loginstyle.css" rel="stylesheet" type="text/css">
	</head>
	<body>	
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }"> 
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}"/>
						</li>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>
			
			<form action="login" method="post"><br />
				<div class="login_id">
				<label for="login_id" >ログインID</label>
				<input name="login_id" value="${login_id}" id="login_id" class="login_id"/> <br />
				</div> 
				
                <div class="password">
				<label for="password" >パスワード</label>
				<input name="password" type="password" value="${password}" id="password" class="password"/> <br />
				</div> <br />
				
				
				<input type="submit" value="ログイン" class="submit"/> <br />
			</form>
		</div>
	   <c:remove var="login_id" scope="session"/>
	</body>
</html>