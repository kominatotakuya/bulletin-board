<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title>ユーザー登録</title>
			<link href="./CSS/userregisterstyle.css" rel="stylesheet" type="text/css">
		</head>
		<body>
		  <h1>新規ユーザー登録</h1>
			<div class="main-contents">
				<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages}" var="message">
								<li><c:out value="${message}" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var ="errorMessages" scope="session" />
				</c:if>
				
				<div class="userregister">
				<form action="userregister" method="post">
					 
					<div class="login_id"> 
					<label for="login_id">ログインＩＤ(半角英数字６～２０文字)</label> <br />
					<input name="login_id" value="${user.login_id}" id="login_id" class="login_id" /> 
					</div>
										
					<div class="password">
					<label for="password">パスワード(半角英数字記号６～２０文字)</label> <br />
					<input name="password" type="password" id="password" class="password" /> 
					</div>
										
					<div class="passwordConfirm">
					<label for="passwordConfirm">パスワード(確認用)</label> <br />
					<input name="passwordConfirm" type="password" id="passwordConfirm" class="passwordConfirm" /> 
					</div>
										
					<div class="name">
					<label for="name">名前(１０文字以内)</label> <br />
					<input name="name" value="${user.name}" id="name" class="name" /> 
					</div>
										
					<div class="store_id">
					<label for="store_id">支店</label> <br />
					<select name="store_id" class="store_id">
					   <c:forEach items="${stores}" var="store" >
					       <c:if test="${store.id == user.store_id }">
                            <option value="${user.store_id}" selected>${store.name}</option> 
                           </c:if>
                            <option value="${store.id}">${store.name}</option> 
                       </c:forEach>
				    </select>
					</div> 
					
					<div class="position_id">
					<label for="position_id">部署</label> <br />
					<select name="position_id" class="position_id">
                       <c:forEach items="${positions}" var="position" >
                            <c:if test="${position.id == user.position_id}">
                                <option value="${user.position_id}" selected>${position.name}</option> 
                            </c:if>
                                <option value="${position.id}">${position.name}</option> 
                       </c:forEach>
                    </select>
                    </div> <br />
                    
                 	<input type="submit" value="登録" class="submit" /><br />
					<a href="usersetting" class="return">戻る</a>
				</form>
				</div>	
			<c:remove var="user" scope="session" />
			</div>
		</body>
</html>