$(function() {
	$('input#submitComment').on('click', function() {
		//送信用データ
		var form = $(this).parents("form.comment");
		var messageId = form.find('.messageId').val();
		var text = form.find('.text').val();
		var comment = { 'messageId': messageId, 'text': text };

		$.ajax({
			dataType: 'json',
			type: "POST",
			url: '/comment',
			data: { comment: JSON.stringify(comment) }
		}).done(function(data, textStatus, jqXHR) {
			//成功時
			if(data.is_success == 'true') {
				form.parents("div.comment").find("div.comments").append(comment);
				


			}
		});

	});
});