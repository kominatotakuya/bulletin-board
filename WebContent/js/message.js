$(function() {
	console.log("OK");
	$('#submitMessage').on('click', function() {

		var form = $(this).parents("form#message");
		var title = $('#title').val();
		var category = $('#category').val();
		var text = $('#text').val();
		console.log(title);
		var message = { 'title': title, 'category': category, 'text': text };
		$.ajax({
			dataType: 'json',
			type: "POST",
			url: 'message',
			data: { message: JSON.stringify(message) }
		}).done(function(data) {
			console.log("SUCCESS");
		}).fail(function(data) {
			console.log("failed");

		});
	});
});