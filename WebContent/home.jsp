<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>ホーム画面</title>
            <link href="./CSS/homestyle.css" rel="stylesheet" type="text/css">
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        </head>
    <body>
       <h1>HOME</h1>
        <div class="main-contents">
            <div class="header">
                    <a href="message" class="message">新規投稿</a> <br/>
                    <c:if test="${ loginUser.position_id == 1 }" >
                       <a href="usersetting" class="usersetting">ユーザー管理</a> <br />
                    </c:if>
                    <form action="./" method="get">
                       <label for="searchCategory" >カテゴリ検索</label> <br />
                       <input type="text" name="searchCategory" value="${searchCategory}" class="searchCategory"/> <br />

                       <label for="dateSearch" >投稿日検索</label> <br />
                       <input type="date" name="startDate" value="${startDate}" class="startDate" /> <br />
                       <input type="date" name="endDate" value="${endDate}" class="endDate" /> <br />
                       <input type="submit" value="検索" class="search" />
                    </form> <br />
                       <a href="./" class="reset">リセット</a>
            </div>
            <a href="logout" class="logout">ログアウト</a>
            <c:remove var="searchCategory" scope="session" />
            <br />

        <div class="messages">
        <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </li>
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
          </c:if>
            <c:forEach items="${messages}" var="message">
                <div class="message">
                    <div class="title"><c:out value="${message.title}" /></div> <br />
                    <div class="category"><c:out value="${message.category}" /></div> <br />
                    <div class="text"><c:forEach var="str" items="${fn:split(message.text, '
                                      ')}"><c:out value="${str}" /><br /></c:forEach> </div>
                    <div class="date"><fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
                    <div class="name"><c:out value="${message.name}" /></div>
                </div>
                    <%-- 投稿の削除 --%>
                <div class="messageDelete">
                    <c:if test="${ message.user_id == loginUser.id }" >
                       <form action="messageDelete" method="post" >
                            <input type="hidden" name="message.id" value="${message.id}" />
                            <input type="submit" value="投稿の削除" onClick="return confirm('本当に削除しますか？')" class="submit" />
                       </form>
                    </c:if>
                    <br />
                </div>
                <div class="comments">
                    <%-- メッセージのIDに対応するコメントの表示 --%>
                    <c:forEach items="${comments}" var="comment">
                       <c:if test="${ message.id == comment.messageId }" >
                               <span class="name"> <c:out value="${comment.name}" /> </span> <br />
                               <span class="text"> <c:forEach var="str" items="${fn:split(comment.text,'
                                                   ')}"><c:out value="${str}" /><br /></c:forEach></span>
                               <span class="created_date"> <fmt:formatDate value="${ comment.created_date }" pattern="yyyy/MM/dd HH:mm:ss" /></span>

                               <div class="commentDelete">
                                   <%-- コメントの削除 --%>
                                   <c:if test="${ comment.userId == loginUser.id }" >
                                        <form action="commentDelete" method="post" >
                                          <input type="hidden" name="comment.id" value="${comment.id}" />
                                          <input type="submit" value="コメントの削除" onClick="return confirm('本当に削除しますか？')" class="submit" />
                                        </form>
                                   </c:if>
                                   <br />
                               </div>
                       </c:if>
                    </c:forEach>
                </div>
                    <br />
                <div class="comment">
                    <%-- コメントの書き込みフォーム --%>
                    <form action="comment" method="post">
                       <input type="hidden" name="message.id" value="${message.id}" />
                       <textarea name="text" cols="100" rows="5" id="text"><c:if test="${message.id == commentMessageId}">${comment}</c:if></textarea> <br />
                       <input type="submit" value="コメント" class="submit"/> <br />
                       <br />
                    </form>
                </div>

            </c:forEach>
        </div>
          <c:remove var="comment" scope="session" />
          <c:remove var="commentMessageId" scope="session" />
        </div>
    </body>
</html>