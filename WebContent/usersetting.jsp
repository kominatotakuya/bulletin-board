<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー管理画面</title>
        <link href="./CSS/usersettingstyle.css" rel="stylesheet" type="text/css">
    </head>
<body>
    <h1>ユーザー管理</h1>
    <div class="main-contents" >
        <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </li>
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
          </c:if>
          
    <div class="header">
            <a href="userregister" class="userregister">ユーザー新規登録</a>
            <a href="./" class="return">戻る</a>
    </div>
    
    <div class="users">
        <table border="1">
            <tr>
                <th>ID</th>
                <th>ログインID</th>
                <th>名前</th>
                <th>支店名</th>
                <th>部署名</th>
                <th>アカウント状態</th>
                <th>アカウント編集</th>
            </tr>
                <c:forEach items="${users}" var="user">
                <tr>
                <td>${user.id}</td> 
                <td>${user.login_id}</td>
                <td>${user.name}</td>
                <td>${user.store_name}</td>
                <td>${user.position_name}</td>
     
                <c:if test="${ user.id != loginUser.id }" >
                    <c:if test="${ user.is_deleted == 1 }" >
                        <td>
                        <form action="userInvalid" method="post" >
                            <input type="hidden" name="invalidUser.id" value="${user.id}" />
                            <input type="submit" value="停止" onClick="return confirm('本当に停止しますか？')" class="userInvalid" />
                        </form>
                        </td>
                    </c:if>
                    <c:if test="${ user.is_deleted == 0 }" >
                        <td>
                        <form action="userValid" method="post" >
                            <input type="hidden" name="validUser.id" value="${user.id}" />
                            <input type="submit" value="復活" onClick="return confirm('本当に復活しますか？')" class="userValid"/>
                  　    </form>
                        </td>
                    </c:if>
                </c:if>
                 <c:if test="${ user.id == loginUser.id }" >
                    <td>ログイン中</td>
                 </c:if>
                <td>
                <a href="useredit?empied=${user.id}" class="useredit" >編集</a> 
                </td>
                </tr>
                
        </c:forEach>
        </table>
    </div>
    
    </div>
</body>
</html>