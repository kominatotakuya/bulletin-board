package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;

public class UsersDao {
	
	// usersetting.jspの表示ユーザーデータ取得
	public List<User> getUsers(Connection connection) {
		
		PreparedStatement ps =null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("users.store_id as store_id, ");
			sql.append("stores.name as store_name, ");
			sql.append("users.position_id as position_id, ");
			sql.append("positions.name as position_name, ");
			sql.append("users.is_deleted as is_deleted ");
			sql.append("FROM users ");
			sql.append("INNER JOIN stores ");
			sql.append("ON users.store_id = stores.id ");
			sql.append("INNER JOIN positions ");
			sql.append("ON users.position_id = positions.id ");
			sql.append("ORDER BY position_id ASC, id ASC ");
			
			ps = connection.prepareStatement(sql.toString());
			
			ResultSet rs = ps.executeQuery();
			List<User> ret = toUsersList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	private List<User> toUsersList(ResultSet rs) throws SQLException {
		
		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String storeName = rs.getString("store_name");
				String positionName = rs.getString("position_name");
				int isDeleted = rs.getInt("is_deleted");
				
				User user = new User();
				user.setId(id);
				user.setLogin_id(loginId);
				user.setName(name);
				user.setStore_name(storeName);
				user.setPosition_name(positionName);
				user.setIs_deleted(isDeleted);
				
				ret.add(user);
				
			}
			return ret;
		} finally {
			close(rs);
		}
	
	}
	
}
