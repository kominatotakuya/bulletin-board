package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exception.SQLRuntimeException;

public class UserValidDao {

		public void valid(Connection connection, int validUserId) {
			
			PreparedStatement ps = null;
			try {
				String sql = "UPDATE users SET is_deleted ='1' WHERE id = ?";
				
				ps = connection.prepareStatement(sql);
				ps.setInt(1, validUserId);
				
				ps.executeUpdate();
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
		}
}
