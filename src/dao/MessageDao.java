package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import exception.SQLRuntimeException;

public class MessageDao {
	
public List<Message> getMessage(Connection connection, String searchCategory, String startDate, String endDate) {
		
		
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.title as title, ");
			sql.append("messages.text as text, ");
			sql.append("messages.category as category, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("users.name as name, ");
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("AND created_date BETWEEN ?");
			sql.append("AND ?");
			if(StringUtils.isBlank(searchCategory) != true) {
				sql.append("AND category LIKE ?");
			}
			sql.append("ORDER BY created_date DESC ");
			
			ps = connection.prepareStatement(sql.toString());
			
			if(StringUtils.isEmpty(startDate) == true && StringUtils.isEmpty(endDate) == true) {
				ps.setString(1, "2000-01-01 00:00:00");
				ps.setString(2, "2100-01-01 23:59:59");
			}
			
			if(StringUtils.isBlank(startDate) != true && StringUtils.isEmpty(endDate) == true) {
				Date sqlStartDate = Date.valueOf(startDate);
				ps.setDate(1, sqlStartDate);
				ps.setString(2, "2100-01-01 23:59:59");
			}
			
			if(StringUtils.isBlank(startDate) == true && StringUtils.isEmpty(endDate) != true) {
				String EndDateTime = endDate + " 23:59:59";
				ps.setString(1, "2000-01-01 00:00:00");
				ps.setString(2, EndDateTime);
			}
			
			if(StringUtils.isBlank(startDate) != true && StringUtils.isEmpty(endDate) != true) {
				Date sqlStartDate = Date.valueOf(startDate);
				String EndDateTime = endDate + " 23:59:59";
				ps.setDate(1, sqlStartDate);
				ps.setString(2, EndDateTime);
			}
			
			if(StringUtils.isBlank(searchCategory)  != true) {
				ps.setString(3, "%" + searchCategory + "%");
			}
			
			ResultSet rs = ps.executeQuery();
			
			List<Message> ret = toMessageList(rs);
			
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	private List<Message> toMessageList(ResultSet rs) throws SQLException {
		
		List<Message> ret = new ArrayList<Message>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String title = rs.getString("title");
				String text = rs.getString("text");
				String category = rs.getString("category");
				int userId = rs.getInt("user_id");
				String name = rs.getString("name");
				Timestamp createdDate = rs.getTimestamp("created_date");
				
				Message message = new Message();
				message.setId(id);
				message.setTitle(title);
				message.setText(text);
				message.setCategory(category);
				message.setUser_id(userId);
				message.setName(name);
				message.setCreated_date(createdDate);
				
				ret.add(message);
				
			} 
			return ret;
		} finally {
			 close(rs);
		}
	}
}
