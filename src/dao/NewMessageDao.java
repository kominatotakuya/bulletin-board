package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Message;
import exception.SQLRuntimeException;

public class NewMessageDao {
	
	public void insert(Connection connection, Message writing) {
		
		PreparedStatement ps =null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages ( ");
			sql.append("title");
			sql.append(", text");
			sql.append(", category");
			sql.append(", user_id");
			sql.append(", created_date");
			sql.append(") VALUES (");
			sql.append("?"); //title
			sql.append(", ?"); //text
			sql.append(", ?"); //category
			sql.append(", ?"); //user_id
			sql.append(", CURRENT_TIMESTAMP"); //created_date
			sql.append(")");
			
			ps = connection.prepareStatement(sql.toString());
			
			ps.setString(1, writing.getTitle());
			ps.setString(2, writing.getText());
			ps.setString(3, writing.getCategory());
			ps.setInt(4, writing.getUser_id());
			
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	
}
