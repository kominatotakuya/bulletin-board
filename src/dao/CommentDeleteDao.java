package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exception.SQLRuntimeException;

public class CommentDeleteDao {
	
	public void delete(Connection connection, int comment) {
		
		PreparedStatement ps = null;
		try {
			String sql = "DELETE FROM comments WHERE id = ?";
			
			ps = connection.prepareStatement(sql);
			ps.setInt(1, comment);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
