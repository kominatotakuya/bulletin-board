package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Store;
import exception.SQLRuntimeException;

public class StoreDao {

	public List<Store> getStores(Connection connection) {
		
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("stores.id as id, ");
			sql.append("stores.name as name ");
			sql.append("FROM stores ");
			
			ps = connection.prepareStatement(sql.toString());
			
			ResultSet rs = ps.executeQuery();
			List<Store> ret = toStoreList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Store> toStoreList(ResultSet rs) throws SQLException {
		
		List<Store> ret = new ArrayList<Store>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				
				Store store = new Store();
				store.setId(id);
				store.setName(name);
				
				ret.add(store);
				
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
