package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exception.SQLRuntimeException;

public class MessageDeleteDao {
	
	public void delete(Connection connection, int message) {
		
		PreparedStatement ps = null;
		try {
			String sql = "DELETE FROM messages WHERE id = ?";
			
			ps = connection.prepareStatement(sql);
			ps.setInt(1, message);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
