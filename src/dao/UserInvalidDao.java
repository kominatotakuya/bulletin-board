package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exception.SQLRuntimeException;

public class UserInvalidDao {

	public void invalid(Connection connection, int invalidUserId) {
		
		PreparedStatement ps = null;
		try {
			String sql = "UPDATE users SET is_deleted ='0' WHERE id = ?";
			
			ps = connection.prepareStatement(sql);
			ps.setInt(1, invalidUserId);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
