package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.NewComment;
import exception.SQLRuntimeException;

public class NewCommentDao {
	
	public void insert(Connection connection, NewComment comment) {
		
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("message_id");
			sql.append(", user_id");
			sql.append(", text");
			sql.append(", created_date");
			sql.append(") VALUES (");
			sql.append(" ?"); //message_id
			sql.append(", ?"); //user_id
			sql.append(", ?"); //text
			sql.append(", CURRENT_TIMESTAMP"); //created_date
			sql.append(")");
			
			ps = connection.prepareStatement(sql.toString());
			
			ps.setString(1, comment.getMessageId());
			ps.setInt(2,  comment.getUserId());
			ps.setString(3, comment.getText());
			
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			 close(ps);
		}
	}
}
