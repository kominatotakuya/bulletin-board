package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {
	// ユーザー新規登録用
	public void insert(Connection connection, User user)  {
		
		PreparedStatement ps = null;
		try {	
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(", password");
			sql.append(", name");
			sql.append(", store_id");
			sql.append(", position_id");
			sql.append(", is_deleted");
			sql.append(") VALUES (");
			sql.append("?"); //login_id
			sql.append(", ?"); //password
			sql.append(", ?"); //name
			sql.append(", ?"); //store_id
			sql.append(", ?"); //position_id
			sql.append(", 1"); //is_deleted
			sql.append(")");
			
			ps = connection.prepareStatement(sql.toString());
			
			ps.setString(1, user.getLogin_id());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getStore_id());
			ps.setInt(5, user.getPosition_id());
			ps.executeUpdate();	
		
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	// ユーザーログイン用
	public User getUser(Connection connection, String login_id, String password) {
		
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ? AND is_deleted =1";
			
			ps = connection.prepareStatement(sql);
			ps.setString(1, login_id);
			ps.setString(2, password);
			
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) { 
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}
	
	private List<User> toUserList(ResultSet rs) throws SQLException {
		
		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int store_id = rs.getInt("store_id");
				int position_id = rs.getInt("position_id");
				int is_deleted =rs.getInt("is_deleted");
				
				User user = new User();
				user.setId(id);
				user.setLogin_id(login_id);
				user.setPassword(password);
				user.setName(name);
				user.setStore_id(store_id);
				user.setPosition_id(position_id);
				user.setIs_deleted(is_deleted);
				
				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
	
	//編集ユーザー呼び出し用
	public User getUser(Connection connection, int editUserId) {
		
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";
			
			ps = connection.prepareStatement(sql);
			ps.setInt(1, editUserId);
			
			ResultSet rs = ps.executeQuery();
			List<User> editUserList = toUserList(rs);
			if(editUserList.isEmpty() == true) {
				return null;
			} else if(2 <= editUserList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return editUserList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public void update(Connection connection, User editUser) {
		
		PreparedStatement ps = null;
		try {
			
			
			StringBuilder sql = new StringBuilder();
			if(StringUtils.isEmpty(editUser.getPassword())) {
				sql.append("UPDATE users SET");
				sql.append("  login_id = ?");
				sql.append(", name = ?");
				sql.append(", store_id = ?");
				sql.append(", position_id = ?");
				sql.append(" WHERE");
				sql.append(" id = ?");
			} else if(!(StringUtils.isEmpty(editUser.getPassword()))) {
					sql.append("UPDATE users SET");
					sql.append("  login_id = ?");	
					sql.append(", password = ?");
					sql.append(", name = ?");
					sql.append(", store_id = ?");
					sql.append(", position_id = ?");
					sql.append(" WHERE");
					sql.append(" id = ?");
			}
			
			ps = connection.prepareStatement(sql.toString());
			
			if(StringUtils.isEmpty(editUser.getPassword())) {
				ps.setString(1, editUser.getLogin_id());
				ps.setString(2, editUser.getName());
				ps.setInt(3, editUser.getStore_id());
				ps.setInt(4, editUser.getPosition_id());
				ps.setInt(5, editUser.getId());
					
			} else if(!(StringUtils.isEmpty(editUser.getPassword()))) {
				ps.setString(1, editUser.getLogin_id());
				ps.setString(2, editUser.getPassword());
				ps.setString(3, editUser.getName());
				ps.setInt(4, editUser.getStore_id());
				ps.setInt(5, editUser.getPosition_id());
				ps.setInt(6, editUser.getId());
			}
			
			int count = ps.executeUpdate();
			if(count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
		
	}
	public User getExistUser(Connection connection, String login_id) {
		
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id =?";
			
			ps = connection.prepareStatement(sql);
			ps.setString(1, login_id);
			
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) { 
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}
}
