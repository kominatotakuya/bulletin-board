package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {

	public List<Comment> getComments(Connection connection) {
		
		PreparedStatement ps =null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id, ");
			sql.append("comments.message_id as message_id, ");
			sql.append("comments.user_id as user_id, ");
			sql.append("users.name as name, ");
			sql.append("comments.text as text, ");
			sql.append("comments.created_date as created_date ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			
			ps = connection.prepareStatement(sql.toString());
			
			ResultSet rs = ps.executeQuery();
			List<Comment> ret = toCommentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	private List<Comment> toCommentList(ResultSet rs) throws SQLException {
		
		List<Comment> ret = new ArrayList<Comment>();
		try {
			while (rs.next()) {
				int id =rs.getInt("id");
				int messageId = rs.getInt("message_id");
				int userId = rs.getInt("user_id");
				String name = rs.getString("name");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");
				
				Comment comment = new Comment();
				comment.setId(id);
				comment.setMessageId(messageId);
				comment.setUserId(userId);
				comment.setName(name);
				comment.setText(text);
				comment.setCreated_date(createdDate);
				
				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
		
		
}
