package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.UserService;

@WebServlet(urlPatterns = { "/userInvalid" })
public class UserInvalidServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		
		int invalidUserId = Integer.parseInt(request.getParameter("invalidUser.id"));
	
		new UserService().invalid(invalidUserId);
		
		response.sendRedirect("usersetting");
	
	}
	
}
