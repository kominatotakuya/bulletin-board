package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Position;
import beans.Store;
import beans.User;
import service.PositionService;
import service.StoreService;
import service.UserService;

@WebServlet(urlPatterns = { "/useredit" })
public class UserEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		
		if(isValid(request, messages) == true) {
			
			int editUserId = Integer.parseInt(request.getParameter("empied"));
			User editUser = new UserService().getUser(editUserId);	
			
			List<Store> stores = new StoreService().getStore();
			request.setAttribute("stores", stores);
			List<Position> positions = new PositionService().getPosition();
			request.setAttribute("positions", positions);
			
			request.setAttribute("editUser", editUser);
			
			request.getRequestDispatcher("useredit.jsp").forward(request, response);
		} else {
			session.setAttribute("errorMessages", messages);
			
			response.sendRedirect("usersetting");
		}
	}
	
	private boolean isValid(HttpServletRequest request, List<String> messages) {
			
		
		if(StringUtils.isBlank(request.getParameter("empied"))) {
			messages.add("不正なアクセスです");
		} else if(request.getParameter("empied").matches("^\\D+$")) {
			messages.add("不正なアクセスです");
		} else {
			
			int editUserId = Integer.parseInt(request.getParameter("empied"));
			User editUser = new UserService().getUser(editUserId);	
			if(editUser == null) {
			messages.add("指定されたユーザーは無効です");
			} 
		}
		
		if(messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		
		if(isValid2(request, messages) == true) {
			
			User editUser = new User();
			editUser.setId(Integer.parseInt(request.getParameter("id")));
			editUser.setLogin_id(request.getParameter("login_id"));
			editUser.setPassword(request.getParameter("password"));
			editUser.setName(request.getParameter("name"));
			editUser.setStore_id(Integer.parseInt(request.getParameter("store_id")));
			editUser.setPosition_id(Integer.parseInt(request.getParameter("position_id")));
		
			new UserService().update(editUser);
		
			response.sendRedirect("usersetting");
		} else {
			
			User editUser = new User();
			editUser.setId(Integer.parseInt(request.getParameter("id")));
			editUser.setLogin_id(request.getParameter("login_id"));
			editUser.setPassword(request.getParameter("password"));
			editUser.setName(request.getParameter("name"));
			editUser.setStore_id(Integer.parseInt(request.getParameter("store_id")));
			editUser.setPosition_id(Integer.parseInt(request.getParameter("position_id")));
			
			List<Store> stores = new StoreService().getStore();
			session.setAttribute("stores", stores);
			List<Position> positions = new PositionService().getPosition();
			session.setAttribute("positions", positions);
			
			session.setAttribute("errorMessages", messages);
			session.setAttribute("editUser", editUser);
			response.sendRedirect("useredit.jsp");
		}
	}
	
	private boolean isValid2(HttpServletRequest request, List<String> messages) {
		
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		String name = request.getParameter("name");
		int store_id = Integer.parseInt(request.getParameter("store_id"));
		int position_id = Integer.parseInt(request.getParameter("position_id"));
		
		User user2 = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
		
		User existUser = new UserService().getExistUser(login_id);
		
		System.out.println(user2.getLogin_id());
		
		if(StringUtils.isBlank(login_id) == true) {
			messages.add("ログインIDを入力してください");
		} else if(login_id.length() < 6 || login_id.length() > 20) {
			messages.add("ログインIDは６文字以上２０文字以下で入力してください");
		} else if(!login_id.matches("^[A-Za-z0-9]+$")) {
			messages.add("ログインIDは半角英数字で入力してください");
		} else if(!(login_id.equals(user2.getLogin_id()))) {
					if(login_id.equals(existUser.getLogin_id())) {
						messages.add("そのログインIDは使用できません");
					} 
		}
					
		if(!(StringUtils.isBlank(password)) && !(StringUtils.isBlank(passwordConfirm))) {
			if(!(password.equals(passwordConfirm))) {
				messages.add("パスワードとパスワード(確認用）が一致しません");
			} else if(password.matches("^\\D+$") || !(password.matches("^.{6,20}$"))) {
				messages.add("パスワードは６文字以上２０文字以下の半角英数字記号で入力してください");
			}
		}
		
		if(!(password.equals(passwordConfirm))) {
			messages.add("パスワードとパスワード(確認用）が一致しません");
		} 
				
		if(StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		} else if(name.length() > 10) {
			messages.add("名前は１０文字以下で入力してください");
		}
		
		if(store_id == 1 && position_id == 2) {
			messages.add("支店と部署の組み合わせが無効です");
		}
		if(store_id != 1 && position_id == 1) {
			messages.add("支店と部署の組み合わせが無効です");
		}
		
		if(messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
