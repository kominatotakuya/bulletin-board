package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.CommentDeleteService;

@WebServlet(urlPatterns = { "/commentDelete" })
public class CommentDeleteServlet extends HttpServlet {
	public static final long serialVersionUID = 1L;
	
	public void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
			
		int deleteComment = Integer.parseInt(request.getParameter("comment.id"));
		
		new CommentDeleteService().delete(deleteComment);
		
		response.sendRedirect("./");
	}
}
