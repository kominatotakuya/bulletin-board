package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Position;
import beans.Store;
import beans.User;
import service.PositionService;
import service.StoreService;
import service.UserService;

@WebServlet(urlPatterns = { "/userregister" })
public class UserRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
			
		List<Store> stores = new StoreService().getStore();
		request.setAttribute("stores", stores);
		List<Position> positions = new PositionService().getPosition();
		request.setAttribute("positions", positions);
		
        request.getRequestDispatcher("userregister.jsp").forward(request, response);
    }
	
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException { 
		
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		
		User user = new User();
		user.setLogin_id(request.getParameter("login_id"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setStore_id(Integer.parseInt(request.getParameter("store_id")));
		user.setPosition_id(Integer.parseInt(request.getParameter("position_id")));
		
		if(isValid(request, messages) == true) {
			
			new UserService().register(user);
			
			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);
			session.setAttribute("user", user);
	
			response.sendRedirect("userregister");
		}
	}
	
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		String name = request.getParameter("name");
		int store_id = Integer.parseInt(request.getParameter("store_id"));
		int position_id = Integer.parseInt(request.getParameter("position_id"));
		
		User existUser = new UserService().getExistUser(login_id);
		
		
		if(StringUtils.isBlank(login_id) == true) {
			messages.add("ログインIDを入力してください");
		} else if(login_id.length() < 6 || login_id.length() > 20) {
			messages.add("ログインIDは６文字以上２０文字以下で入力してください");
		} else if(!login_id.matches("^[A-Za-z0-9]+$")) {
			messages.add("ログインIDは半角英数字で入力してください");
		} else if(existUser != null) {
				if(login_id.equals(existUser.getLogin_id())) {
				messages.add("そのログインIDは使用できません");
			}
		}
			
		if(StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");
		} else if(!(password.equals(passwordConfirm))) {
			messages.add("パスワードとパスワード(確認用）が一致しません");
		} else if(password.matches("^\\D+$") || !(password.matches("^.{6,20}$"))) {
			messages.add("パスワードは６文字以上２０文字以下の半角英数字記号で入力してください");
		} 
		
		if(StringUtils.isBlank(name) == true) {
			messages.add("名前を入力して下さい");
		} else if(name.length() > 10) {
			messages.add("名前は１０文字以下で入力してください"); 
		}
		
		if(store_id == 1 && position_id == 2) {
			messages.add("支店と部署の組み合わせが無効です");
		}
		if(store_id != 1 && position_id == 1) {
			messages.add("支店と部署の組み合わせが無効です");
		}
		if(messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
	
}
