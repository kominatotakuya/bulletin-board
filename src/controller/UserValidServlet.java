package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.UserService;

@WebServlet(urlPatterns = { "/userValid" })
public class UserValidServlet extends HttpServlet {
	public static final long serialVersionUID = 1L;
	
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		int validUserId = Integer.parseInt(request.getParameter("validUser.id"));
		
		new UserService().valid(validUserId);
		
		response.sendRedirect("usersetting");
	}
}
