package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.NewComment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class NewCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {


			User user = (User) session.getAttribute("loginUser");
			NewComment comment = new NewComment();

			comment.setUserId(user.getId());
			comment.setMessageId(request.getParameter("message.id"));
			comment.setText(request.getParameter("text"));

			new CommentService().register(comment);

			response.sendRedirect("./");
		} else {
			String comment = request.getParameter("text");
			session.setAttribute("comment", comment);
			int commentMessageId = Integer.parseInt(request.getParameter("message.id"));
			session.setAttribute("commentMessageId", commentMessageId);
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String comment = request.getParameter("text");

		if (StringUtils.isBlank(comment) == true) {
			messages.add("本文を入力してください");
		} else {
			if (500 < comment.length()) {
				messages.add("500文字以下で入力してください");
			}
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
