package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Comment;
import beans.Message;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

			String searchCategory = request.getParameter("searchCategory");
			String startDate = request.getParameter("startDate");
			String endDate = request.getParameter("endDate");

			HttpSession session = request.getSession();
			List<String> errorMessages = new ArrayList<String>();

			// 投稿一覧のリスト
			List<Message> messages = new MessageService().getMessage(searchCategory, startDate, endDate);
			request.setAttribute("messages", messages);
			// コメント一覧のリスト
			List<Comment> comments = new CommentService().getComment();
			request.setAttribute("comments", comments);

			if(messages.size() == 0) {
				errorMessages.add("検索結果は０件です");
				session.setAttribute("errorMessages", errorMessages);
				session.setAttribute("searchCategory", searchCategory);
				request.getRequestDispatcher("/home.jsp").forward(request, response);
			}


			if(searchCategory != null ) {
				if(searchCategory.length() > 10) {
					errorMessages.add("カテゴリーは１０文字以下で入力してください");
					session.setAttribute("errorMessages", errorMessages);
					session.setAttribute("searchCategory", searchCategory);
					request.getRequestDispatcher("/home.jsp").forward(request, response);
				}
			}
			session.setAttribute("startDate", startDate);
			session.setAttribute("endDate", endDate);
			session.setAttribute("searchCategory", searchCategory);
			request.getRequestDispatcher("/home.jsp").forward(request, response);
	}
}
