package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {
	public static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("/message.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if (isValid(request, messages)  == true) {

			User user = (User) session.getAttribute("loginUser");

			Message message = new Message();
			message.setTitle(request.getParameter("title"));
			message.setText(request.getParameter("text"));
			message.setCategory(request.getParameter("category"));
			message.setUser_id(user.getId());

			new MessageService().register(message);

			response.sendRedirect("./");
		} else {
			String title = request.getParameter("title");
			String category = request.getParameter("category");
			String text = request.getParameter("text");
			session.setAttribute("title", title);
			session.setAttribute("category", category);
			session.setAttribute("text", text);

			session.setAttribute("errorMessages", messages);
			response.sendRedirect("message");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String title = request.getParameter("title");
		String text = request.getParameter("text");
		String category = request.getParameter("category");

		if(StringUtils.isBlank(title) == true) {
			messages.add("タイトルを入力してください");
		} else {
			if(30 < title.length()) {
				messages.add("タイトルは30文字以下で入力して下さい");
			}
		}
		if(StringUtils.isBlank(category)) {
			messages.add("カテゴリーを入力してください");
		} else {
			if(10 < category.length()) {
				messages.add("カテゴリーは10文字以下で入力して下さい");
			}
		}
		if(StringUtils.isBlank(text)) {
			messages.add("テキストを入力してください");
		} else {
			if(1000 < text.length()) {
				messages.add("テキストは1000文字以下で入力して下さい");
			}
		}
		if(messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
