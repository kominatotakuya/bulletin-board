package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class ManagementFilter implements Filter {
	
	public void doFilter(ServletRequest request, 
			ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		String target = ((HttpServletRequest) request).getRequestURI();
		HttpSession session = ((HttpServletRequest) request).getSession();
		List<String> messages = new ArrayList<String>();
		
		if(target.contains("user")) {
			if(session == null) {
				((HttpServletResponse) response).sendRedirect("./");
				return;
			} else {
				User user = (User) session.getAttribute("loginUser");
				int authority = user.getPosition_id();
				
				if(authority == 1) {
				} else {
					messages.add("権限がありません");
					session.setAttribute("errorMessages", messages);
					((HttpServletResponse) response).sendRedirect("./");
					return;
				}
			}
		}
		chain.doFilter(request, response);
	}
	public void init(FilterConfig filterConfig) throws ServletException {
	}
	
	public void destroy() {
	}
}