package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/*")
public class LoginFilter implements Filter {
	
	public void doFilter(ServletRequest request, 
			ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		String target = ((HttpServletRequest) request).getRequestURI();
		HttpSession session = ((HttpServletRequest) request).getSession();
		
		List<String> messages = new ArrayList<String>();
		
		if(target.contains("style")) {
		}
		if(target.contains("login"))  {
		} else {
			if(session == null) {
				((HttpServletResponse) response).sendRedirect("login");
				return;
			} else {
				Object loginCheck =  session.getAttribute("loginUser");
				if(loginCheck == null) {
					messages.add("ログインしてください");
					session.setAttribute("errorMessages", messages);
					((HttpServletResponse) response).sendRedirect("login");
					return;
				}
			}
		}
		chain.doFilter(request, response);
	}
		
	public void init(FilterConfig filterConfig) throws ServletException {
	}
		
	public void destroy() {
	}
}
