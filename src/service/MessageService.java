package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Message;
import dao.MessageDao;
import dao.NewMessageDao;

public class MessageService {
	public void register(Message message) {
		
		Connection connection =null;
		try {
			connection = getConnection();
			
			NewMessageDao messageDao = new NewMessageDao();
			messageDao.insert(connection, message);
			
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	public List<Message> getMessage(String searchCategory, String startDate, String endDate) {
		
		Connection connection = null;
		try {
			connection = getConnection();
			
			MessageDao messageDao = new MessageDao();
			List<Message> ret = messageDao.getMessage(connection, searchCategory, startDate, endDate);
			
			commit(connection);
			
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
}
