package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import beans.NewComment;
import dao.CommentDao;
import dao.NewCommentDao;

public class CommentService {

	public void register(NewComment comment) {

		Connection connection = null;
		try {
			connection = getConnection();

			NewCommentDao commentDao= new NewCommentDao();
			commentDao.insert(connection, comment);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//　コメントを取得するためのコード
		public List<Comment> getComment() {

			Connection connection = null;
			try {
				connection = getConnection();

				CommentDao commentDao = new CommentDao();
				List<Comment> ret = commentDao.getComments(connection);
				
				commit(connection);

				return ret;
			} catch (RuntimeException e) {
				rollback(connection);
				throw e;
			} catch (Error e) {
				rollback(connection);
				throw e;
			} finally {
				close(connection);
			}


		}

}
