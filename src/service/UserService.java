package service;


import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import dao.UserDao;
import dao.UserInvalidDao;
import dao.UserValidDao;
import dao.UsersDao;
import utils.CipherUtil;

public class UserService {
	// ユーザー登録
	public void register(User user) {
	
		Connection connection = null;
		try {
			connection = getConnection();
			
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);
			
			UserDao userDao =new UserDao();
			userDao.insert(connection, user);
			
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	// ユーザー一覧の取得
	public List<User> getUsers() {
		
		Connection connection = null;
		try {
			connection =getConnection();
			
			UsersDao usersDao = new UsersDao();
			List<User> ret = usersDao.getUsers(connection);
		
			commit(connection);
			
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	// ユーザー停止
	public void invalid(int invalidUserId) {
		
		Connection connection = null;
		try {
			connection  = getConnection();
			
			UserInvalidDao userInvalidDao = new UserInvalidDao();
			userInvalidDao.invalid(connection, invalidUserId);
			
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			throw e;
		} finally {
			close(connection);
		}
		
	}
	// ユーザー復活
	public void valid(int validUserId) {
		
		Connection connection = null;
		try {
			connection = getConnection();
			
			UserValidDao userValidDao = new UserValidDao();
			userValidDao.valid(connection, validUserId);
			
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			throw e;
		} finally {
			close(connection);
		}
	}
	//編集ユーザー呼び出し用
	public User getUser(int editUserId) {
		
		Connection connection = null;
		try {
			connection = getConnection();
			
			UserDao userDao = new UserDao();
			User user = userDao.getUser(connection, editUserId);
			
			commit(connection);
			
			return user;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(User editUser) {
		
		Connection connection = null;
		try {
			connection = getConnection();
			
			if(!(StringUtils.isEmpty(editUser.getPassword()))) {
					String encPassword = CipherUtil.encrypt(editUser.getPassword());
					editUser.setPassword(encPassword);
			}
			UserDao userDao = new UserDao();
			userDao.update(connection, editUser);
			
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
		
	}
	public User getExistUser(String login_id) {
		
		Connection connection = null;
		try {
			connection = getConnection();
			
			UserDao userDao = new UserDao();
			User user = userDao.getExistUser(connection, login_id);
			
			commit(connection);
			
			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
}
