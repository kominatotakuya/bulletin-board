package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import dao.MessageDeleteDao;

public class MessageDeleteService {
	public void delete(int message) {
		
		Connection connection = null;
		try {
			connection = getConnection();
			
			MessageDeleteDao messageDeleteDao = new MessageDeleteDao();
			messageDeleteDao.delete(connection, message);
			
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
