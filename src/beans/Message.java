package beans;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int Id;
	private String title;
	private String text;
	private String category;
	private Date createdDate;
	private int userId;
	private String name;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Date getCreated_date() {
		return createdDate;
	}
	public void setCreated_date(Date created_Date) {
		this.createdDate = created_Date;
	}
	public int getUser_id() {
		return userId;
	}
	public void setUser_id(int user_Id) {
		this.userId = user_Id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
